from setuptools import setup, find_packages
from pip._internal.req import parse_requirements

install_reqs = parse_requirements('addons-requirements.txt', session=False)

# reqs is a list of requirement
# e.g. ['django==1.5.1', 'mezzanine==1.4.6']
try:
    reqs = [str(ir.req) for ir in install_reqs]
except:
    reqs = [str(ir.requirement) for ir in install_reqs]

setup(
    name="rasa_addons",
    version="0.0.1",
    author="Botfront",
    description="Rasa Addons - Components for medical chatbot",
    long_description_content_type="text/markdown",
    install_requires=reqs,
    packages=find_packages(include=["rasa_addons*"]),
    licence="Apache 2.0",
    url="https://vinbrain.net",
    author_email="namph@vinbrain.net",
)
