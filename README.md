## VMA
A virtual medical assistant to help users book doctor appointments, collect symptoms, diagnose diseases...

## Setup development evironment

Clone config file from `.env.template` and edit `.env` file

```bash
cp .env.template .env
export $(cat .env)
```

### Prepare model file

Download trained model from [drive](https://drive.google.com/file/d/12ABGhkfXcy8GhHoJFUrGmP1qRFhU0izJ/view?usp=sharing) to `models/` or train on local machine by command

```bash
docker-compose run -u 0 --no-deps --rm --entrypoint rasa rasa-production train
```

### Run all services

Run all services in one terminal screen by command

```bash
docker-compose up
```

You can also run individual service in a separated screen for better logging

```bash
# run external services in background
docker-compose up -d rasa-x

# Rasa core service
docker-compose up rasa-production

# Action service
docker-compose up app
```

------------------------------------------------------------
## Development

### Core service

If only config files (`endpoints`, `credentials`) and/or models were changed, you can update Rasa core service just by restart the `rasa-production` service.

```bash
docker-compose restart rasa-production
```

But if you update Rasa or custom components’ versions, the `rasa-production` service must be rebuilt to reflect the changes

```bash
docker-compose rm -sfv rasa-production
docker-compose up --no-deps -d --build rasa-production
```

### Action service

Update action service is same as core service.

Update action source code, restart only:

```bash
docker-compose restart app
```

Update rasa-sdk version or action requirements, stop and rebuild:
```bash
docker-compose rm -sfv app
docker-compose up --no-deps -d --build app
```