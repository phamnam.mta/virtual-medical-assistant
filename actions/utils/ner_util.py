import logging
from typing import Dict, Text, List, Any, Callable
from vma_nlu.ner.extractor import Extractor

logger = logging.getLogger(__name__)
extractor = Extractor()

def extract_person_name(text: Text):
    username = text
    output = extractor.extract_person_name(text)
    if output.get("entities"):
        username = output.get("entities")[0].get("value", None)
    return username

def extract_time(text: Text):
    time = None
    output = extractor.extract_time(text)
    if output.get("entities"):
        temp = output.get("entities")[0].get("value", None)
        time = f'{temp[0]}:{temp[1]}'
    return time

def extract_date(text: Text):
    output = extractor.extract_date(text)
    date_output = None
    if output.get("entities"):
        date = output.get("entities")[0].get("value", None)
        if date:
            first_date_extracted = date[0]
            date_output = f'{first_date_extracted[1]}/{first_date_extracted[2]}/{first_date_extracted[3]}'
    return date_output
