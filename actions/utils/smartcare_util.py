import os
import logging
import json
import uuid
from typing import Dict, Text, List, Any, Callable

logger = logging.getLogger(__name__)

WORK_DIR = os.path.dirname(os.path.abspath(__file__))
DB_PATH = os.path.join(WORK_DIR, "db.json")

def get_user_by_parent(parent_id: Text):
    with open(DB_PATH, "r") as f:
        data = json.load(f)
        users = [x for x in data["users"] if x["parent_id"] == parent_id]
        users.sort(key=lambda x: 0 if x["id"] == x["parent_id"] else 1)
        return users[1:]

def get_user(user_id: Text):
    with open(DB_PATH, "r") as f:
        data = json.load(f)
        user = next((x for x in data["users"] if x["id"] == user_id), None)
        return user

def get_schedules(parent_id: Text, doctor_name: Text=None, date: Text=None, time: Text=None):
    with open(DB_PATH, "r") as f:
        data = json.load(f)
        schedules = [s for s in data["schedules"] if s["parent_id"] == parent_id]
        if doctor_name:
            schedules = [s for s in schedules if s["doctor_name"] == doctor_name]
        if date:
            schedules = [s for s in schedules if s["date"] == date]
        if time:
            schedules = [s for s in schedules if s["time"] == time]
        return schedules

def get_schedule(id: Text):
    with open(DB_PATH, "r") as f:
        data = json.load(f)
        schedule = next((x for x in data["schedules"] if x["id"] == id), None)
        return schedule

def get_doctors():
    with open(DB_PATH, "r") as f:
        data = json.load(f)
        return data["doctors"]

def insert_schedule(schedule):
    with open(DB_PATH, "r+") as f:
        data = json.load(f)
        schedule["id"] = str(uuid.uuid4())

        data["schedules"].append(schedule)
        f.seek(0)  # rewind
        json.dump(data, f, ensure_ascii=False)
        f.truncate()
        return schedule["id"]

def update_schedule(schedule: Dict):
    with open(DB_PATH, "r+") as f:
        data = json.load(f)
        _id = schedule.get("id", None)
        position = [i for i, x in enumerate(data["schedules"]) if x["id"] == _id]
        if position:
            data["schedules"][position[0]] = schedule
        f.seek(0)  # rewind
        json.dump(data, f, ensure_ascii=False)
        f.truncate()
        return schedule["id"]

def delete_schedule(id: Text):
    with open(DB_PATH, "r+") as f:
        data = json.load(f)
        position = [i for i, x in enumerate(data["schedules"]) if x["id"] == id]
        if position:
            data["schedules"].remove(data["schedules"][position[0]])
        f.seek(0)  # rewind
        json.dump(data, f, ensure_ascii=False)
        f.truncate()
        return id

def update_user(user: Dict):
    with open(DB_PATH, "r+") as f:
        data = json.load(f)
        _id = user.get("id", None)
        position = [i for i, x in enumerate(data["users"]) if x["id"] == _id]
        if position:
            data["users"][position[0]] = user
        f.seek(0)  # rewind
        json.dump(data, f, ensure_ascii=False)
        f.truncate()
        return user["id"]

def insert_user(user_info: Dict):
    with open(DB_PATH, "r+") as f:
        data = json.load(f)
        user_info["id"] = user_info.get("id", str(uuid.uuid4()))

        data["users"].append(user_info)
        f.seek(0)  # rewind
        json.dump(data, f, ensure_ascii=False)
        f.truncate()
        return user_info["id"]

def get_time_slots(department: Text, date: Text, time: Text, doctor_name: Text, doctor_id: Text=None):
    try:
        with open(DB_PATH, "r") as f:
            data = json.load(f)
            doctors = data["doctors"]
            if doctor_name:
                data["doctors"][0]["doctor_name"] = doctor_name
                doctors = [data["doctors"][0]]

            # Temporarily display the date/time that NER extracted
            if time or date:
                for i, d in enumerate(doctors):
                    doctors[i]["time_slots"] = []
                    doctors[i]["time_slots"].append({"time": time, "date": date, "fee": "500.000 VND"})

        return doctors

    except Exception as ex:
        logger.exception(ex)