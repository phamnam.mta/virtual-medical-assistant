from typing import Text, List, Optional, Dict, Any

from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from actions.utils.common import get_bot_utter
from actions.utils.ner_util import extract_date, extract_person_name
class ValidateUserInfoForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_user_info_form"

    def get_value(self, intent: Text):
        if intent == "agree":
            return '1'
        if intent == "disagree":
            return '0'
        if intent == "ignore":
            return '2'
        return None


    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Optional[List[Text]]:
        required_slots = slots_mapped_in_domain
        username = tracker.get_slot("username")
        gender = tracker.get_slot("gender")
        birthday = tracker.get_slot("birthday")
        
        if username and gender and birthday:
            required_slots = slots_mapped_in_domain + ["smoker", "diabetes", "hypertension", "heart_problems"]

        return required_slots

    async def extract_smoker(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("smoker"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_smoker":
            last_intent = tracker.get_intent_of_latest_message()
            return {"smoker": self.get_value(last_intent)}
        return {}

    async def extract_diabetes(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("diabetes"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_diabetes":
            last_intent = tracker.get_intent_of_latest_message()
            return {"diabetes": self.get_value(last_intent)}
        return {}

    async def extract_hypertension(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("hypertension"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_hypertension":
            last_intent = tracker.get_intent_of_latest_message()
            return {"hypertension": self.get_value(last_intent)}
        return {}

    async def extract_heart_problems(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("heart_problems"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_heart_problems":
            last_intent = tracker.get_intent_of_latest_message()
            return {"heart_problems": self.get_value(last_intent)}
        return {}

    async def extract_username(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("username"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_for_username":
            text_of_last_user_message = tracker.latest_message.get("text")
            username = extract_person_name(text_of_last_user_message)
            return {"username": username}
        return {}

    async def extract_birthday(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("birthday"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_for_birthday":
            text_of_last_user_message = tracker.latest_message.get("text")
            birthday = extract_date(text_of_last_user_message)
            return {"birthday": birthday}
        return {}
