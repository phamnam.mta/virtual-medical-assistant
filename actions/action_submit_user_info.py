import logging

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import Restarted

from actions.utils.smartcare_util import insert_user

logger = logging.getLogger(__name__)

class ActionSubmitUserInfo(Action):

    def name(self) -> Text:
        return "action_submit_user_info"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            user_id = tracker.current_state()["sender_id"]
            user_information = {
                "id": user_id,
                "parent_id": user_id,
                "username": tracker.get_slot("username"),
                "gender" : tracker.get_slot("gender"),
                "birthday" : tracker.get_slot("birthday"),
                "smoker" : tracker.get_slot("smoker"),
                "diabetes" : tracker.get_slot("diabetes"),
                "hypertension" : tracker.get_slot("hypertension"),
                "heart_problems" : tracker.get_slot("heart_problems")
            }
            insert_user(user_information)
            dispatcher.utter_message(response="utter_info_collected")
            return [Restarted()]
        except Exception as ex:
            logger.exception(ex)
            return []