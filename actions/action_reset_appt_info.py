import logging

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset, Restarted

from actions.utils.constants import SCHEDULE_FIELD

logger = logging.getLogger(__name__)


class ActionResetApptInfo(Action):

    def name(self) -> Text:
        return "action_reset_appt_info"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            # events = []
            # for f in SCHEDULE_FIELD:
            #     events.append(SlotSet(f, None))
            # events.append(SlotSet("info_changed", None))
            return [Restarted()]
        except Exception as ex:
            logger.exception(ex)
            return []