import logging

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import ActiveLoop, FollowupAction, SlotSet, Restarted

from actions.utils.constants import GENERIC_USER
from actions.utils.smartcare_util import get_user

logger = logging.getLogger(__name__)


class ActionGreet(Action):

    def name(self) -> Text:
        return "action_greet"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            user_id = tracker.current_state()["sender_id"]
            user_info = get_user(user_id)
            if not user_info:
                dispatcher.utter_message(response="utter_get_started")
                return [Restarted(), SlotSet("user_id", user_id), FollowupAction("user_info_form"), ActiveLoop("user_info_form")]
            username = user_info.get("username", GENERIC_USER)
            dispatcher.utter_message(response="utter_greet", username=username)
            return [Restarted()]
        except Exception as ex:
            logger.exception(ex)
            return []