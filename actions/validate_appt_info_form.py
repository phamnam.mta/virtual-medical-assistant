from typing import Text, List, Optional, Dict, Any
from rasa_sdk import Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.forms import FormValidationAction

from actions.utils.common import get_bot_utter
from actions.utils.ner_util import extract_date, extract_person_name, extract_time

class ValidateApptInfoForm(FormValidationAction):

    def name(self) -> Text:
        return "validate_appt_info_form"


    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Optional[List[Text]]:
        additional_slots = []
        if tracker.slots.get("with_doctor") == False:
            additional_slots += ["date", "time"]
        if tracker.slots.get('with_doctor') == True:
            additional_slots.append("doctor_name")
        return additional_slots + slots_mapped_in_domain

    async def extract_username(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("username"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_for_username":
            text_of_last_user_message = tracker.latest_message.get("text")
            username = extract_person_name(text_of_last_user_message)
            return {"username": username}
        return {}

    async def extract_birthday(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("birthday"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_for_birthday":
            text_of_last_user_message = tracker.latest_message.get("text")
            birthday = extract_date(text_of_last_user_message)
            return {"birthday": birthday}
        return {}

    async def extract_time(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("time"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_time":
            text_of_last_user_message = tracker.latest_message.get("text")
            time = extract_time(text_of_last_user_message)
            return {"time": time}
        return {}

    async def extract_date(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("date"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_date":
            text_of_last_user_message = tracker.latest_message.get("text")
            date = extract_date(text_of_last_user_message)
            return {"date": date}
        return {}

    async def extract_doctor_name(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("doctor_name"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_for_doctor_name":
            text_of_last_user_message = tracker.latest_message.get("text")
            doctor_name = extract_person_name(text_of_last_user_message)
            return {"doctor_name": doctor_name}
        return {}

class ValidateChangeApptInfoForm(FormValidationAction):

    def name(self) -> Text:
        return "validate_change_appt_info_form"


    async def required_slots(
        self,
        slots_mapped_in_domain: List[Text],
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Optional[List[Text]]:
        additional_slots = []
        if tracker.slots.get("info_changed") == 'other_date':
            additional_slots += ["date", "time", "time_slot"]
        if tracker.slots.get('info_changed') == 'other_doctor':
            additional_slots += ["doctor_name", "time_slot"]
        if tracker.slots.get('info_changed') == 'patient_info':
            additional_slots += ["username", "gender", "birthday"]
        if tracker.slots.get('info_changed') == 'symptom':
            additional_slots += ["note"]
        return additional_slots + slots_mapped_in_domain

    async def extract_date(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("date"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_date":
            text_of_last_user_message = tracker.latest_message.get("text")
            date = extract_date(text_of_last_user_message)
            return {"date": date}
        return {}

    async def extract_time(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("time"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_time":
            text_of_last_user_message = tracker.latest_message.get("text")
            time = extract_time(text_of_last_user_message)
            return {"time": time}
        return {}

    async def extract_doctor_name(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("doctor_name"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_for_doctor_name":
            text_of_last_user_message = tracker.latest_message.get("text")
            doctor_name = extract_person_name(text_of_last_user_message)
            return {"doctor_name": doctor_name}
        return {}

    async def extract_username(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("username"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_for_username":
            text_of_last_user_message = tracker.latest_message.get("text")
            username = extract_person_name(text_of_last_user_message)
            return {"username": username}
        return {}

    async def extract_birthday(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("birthday"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_for_birthday":
            text_of_last_user_message = tracker.latest_message.get("text")
            birthday = extract_date(text_of_last_user_message)
            return {"birthday": birthday}
        return {}

    async def extract_note(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> Dict[Text, Any]:
        if tracker.slots.get("note"):
            return {}
        bot_utter = get_bot_utter(tracker)
        if bot_utter and bot_utter[0] == "utter_ask_note":
            text_of_last_user_message = tracker.latest_message.get("text")
            return {"note": text_of_last_user_message}
        return {}