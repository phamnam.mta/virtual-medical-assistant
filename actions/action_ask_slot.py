import logging
from typing import Dict, Text, List, Any

from rasa_sdk import Tracker
from rasa_sdk.events import EventType
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action

from actions.utils.smartcare_util import get_time_slots, get_doctors, get_user_by_parent
from actions.utils.common import time_slots_display, doctors_display
from actions.utils.constants import DEFAULT_ID, FOR_OTHER_TEXT, FOR_YOU_TEXT

logger = logging.getLogger(__name__)

class ActionAskTimeSlot(Action):
    def name(self) -> Text:
        return "action_ask_time_slot"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        doctor_time_slot = get_time_slots(tracker.get_slot("department"), tracker.get_slot(
            "date"), tracker.get_slot("time"), tracker.get_slot("doctor_name"), tracker.get_slot("doctor_id"))
        time_slots = time_slots_display(doctor_time_slot)
        message = {
            "type": "template",
            "payload": {
                    "template_type": "generic",
                    "elements": time_slots
            }
        }
        dispatcher.utter_message(response="utter_ask_suitable_time")
        dispatcher.utter_message(attachment=message)
        return []
class ActionAskUserId(Action):

    def name(self) -> Text:
        return "action_ask_user_id"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            parent_id = tracker.current_state()["sender_id"]
            users = get_user_by_parent(parent_id)
            buttons = []
            buttons.append({"payload": f'/inform{{"user_id": "{parent_id}"}}', "title": "Tôi"})
            for u in users:
                buttons.append({"payload": f'/inform{{"user_id": "{u["id"]}"}}', "title": f'{u["username"]}'})
            buttons.append({"payload": f'/inform{{"user_id": "{DEFAULT_ID}"}}', "title": "Người khác"})
            dispatcher.utter_message(response="utter_who_book_for", buttons=buttons)
            return []
        except Exception as ex:
            logger.exception(ex)
            return []
class ActionAskUserName(Action):
    def name(self) -> Text:
        return "action_ask_username"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        obj = FOR_YOU_TEXT
        parent_id = tracker.current_state()["sender_id"]
        if tracker.get_slot('user_id') != parent_id:
            obj = FOR_OTHER_TEXT
        dispatcher.utter_message(response="utter_ask_for_username", obj=obj)
        return []


class ActionAskGender(Action):
    def name(self) -> Text:
        return "action_ask_gender"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        obj = FOR_YOU_TEXT
        parent_id = tracker.current_state()["sender_id"]
        if tracker.get_slot('user_id') != parent_id:
            obj = FOR_OTHER_TEXT
        dispatcher.utter_message(response="utter_ask_for_gender", obj=obj)
        return []


class ActionAskBirthday(Action):
    def name(self) -> Text:
        return "action_ask_birthday"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        obj = FOR_YOU_TEXT
        parent_id = tracker.current_state()["sender_id"]
        if tracker.get_slot('user_id') != parent_id:
            obj = FOR_OTHER_TEXT
        dispatcher.utter_message(response="utter_ask_for_birthday", obj=obj)
        return []
class ActionAskDoctorName(Action):
    def name(self) -> Text:
        return "action_ask_doctor_name"

    def run(
        self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict
    ) -> List[EventType]:
        doctors = get_doctors()
        doctor_to_display = doctors_display(doctors)
        message = {
            "type": "template",
            "payload": {
                    "template_type": "generic",
                    "elements": doctor_to_display
            }
        }
        dispatcher.utter_message(response="utter_ask_for_doctor_name")
        dispatcher.utter_message(attachment=message)
        return []
