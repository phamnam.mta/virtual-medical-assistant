import logging
import uuid
import copy

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset, FollowupAction

from actions.utils.smartcare_util import get_schedule, update_schedule, update_user, get_user
from actions.utils.constants import SCHEDULE_FIELD, USER_FIELD

logger = logging.getLogger(__name__)


class ActionChangeAppt(Action):

    def name(self) -> Text:
        return "action_change_appt"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            _id = tracker.slots.get("appt_id")
            schedule_old = get_schedule(_id)
            if not schedule_old:
                return []
            if tracker.get_slot("info_changed") == "patient_info":
                user_id = schedule_old.get("user_id")
                user_info = get_user(user_id)
                for f in USER_FIELD:
                    user_info[f] = tracker.get_slot(f)
                update_user(user_info)

            schedule_new = copy.deepcopy(schedule_old)
            for f in SCHEDULE_FIELD:
                schedule_new[f] = tracker.slots.get(f)

            schedule_id = update_schedule(schedule_new)
            if schedule_id:
                fee_old_text = schedule_old.get("fee")
                fee_text = schedule_new.get("fee")
                if fee_old_text and fee_text:
                    fee_old = int(fee_old_text.split(" ")[0].replace(".",""))
                    fee = int(fee_text.split(" ")[0].replace(".",""))
                    if fee_old > fee:
                        reduction = fee_old - fee
                        dispatcher.utter_message(response="utter_fee_reduction", fee_reduction=reduction)
                    elif fee_old < fee:
                        increase = fee - fee_old
                        dispatcher.utter_message(response="utter_fee_increase", fee_increase=increase)
                    else:
                        dispatcher.utter_message(response="utter_change_appt_successful")
                else:
                    dispatcher.utter_message(response="utter_change_appt_successful")
            return []
        except Exception as ex:
            logger.exception(ex)
            return []

class ActionSlotSetChangeAppt(Action):

    def name(self) -> Text:
        return "action_slot_set_change_appt"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            _id = tracker.slots.get("appt_id")
            schedule_old = get_schedule(_id)
            if not schedule_old:
                return []

            events = []
            for f in SCHEDULE_FIELD:
                slot = tracker.get_slot(f)
                if not slot:
                    events.append(SlotSet(f, schedule_old[f]))
            return events
        except Exception as ex:
            logger.exception(ex)
            return []