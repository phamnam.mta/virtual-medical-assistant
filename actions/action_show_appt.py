import logging
import uuid

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset, FollowupAction

from actions.utils.common import get_last_appt_intent, elements_to_display
from actions.utils.constants import ACTION_CHANGE_TEXT, ACTION_CANCEL_TEXT, APPT_INFO_SLOTS
from actions.utils.smartcare_util import get_schedules

logger = logging.getLogger(__name__)

class ActionChangeAppt(Action):

    def name(self) -> Text:
        return "action_show_appt"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            parent_id = tracker.current_state()["sender_id"]
            doctor_name = tracker.slots.get("doctor_name", None)
            date = tracker.slots.get("date", None)
            time = tracker.slots.get("time", None)
            appt_list = get_schedules(parent_id, doctor_name, date,time)
            if not appt_list and (doctor_name or date or time):
                last_appt_intent = get_last_appt_intent(tracker)
                buttons = [{"payload": f'/{last_appt_intent}', "title": "Danh sách lịch hẹn"}]
                dispatcher.utter_message(response="utter_no_matching", buttons=buttons)
                events = []
                for s in APPT_INFO_SLOTS:
                    events.append(SlotSet(s, None))
                return events
            elif not appt_list:
                dispatcher.utter_message(response="utter_no_appt")
                return []

            if len(appt_list) == 1:
                return [SlotSet("appt_id", appt_list[0].get("id"))]

            last_appt_intent = get_last_appt_intent(tracker)
            action_text = ACTION_CHANGE_TEXT
            if last_appt_intent == "cancel_appt":
                action_text = ACTION_CANCEL_TEXT

            elements = elements_to_display(appt_list, last_appt_intent)
            message = {
                "type": "template",
                "payload": {
                        "template_type": "generic",
                        "elements": elements
                }
            }
            dispatcher.utter_message(response='utter_ask_change_appt', number_appt=len(appt_list), action=action_text)
            dispatcher.utter_message(attachment=message)
            return []
        except Exception as ex:
            logger.exception(ex)
            return []