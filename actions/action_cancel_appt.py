import logging
import uuid

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset, FollowupAction

from actions.utils.smartcare_util import delete_schedule

logger = logging.getLogger(__name__)


class ActionCancelAppt(Action):

    def name(self) -> Text:
        return "action_cancel_appt"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            _id = tracker.slots.get("appt_id")
            if _id:
                delete_schedule(_id)
                dispatcher.utter_message(response="utter_cancel_appt_successful")
            return []
        except Exception as ex:
            logger.exception(ex)
            return []

