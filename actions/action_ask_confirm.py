import logging
from typing import Dict, Text, List, Any

from rasa_sdk import Tracker
from rasa_sdk.events import EventType, SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk import Action

from actions.utils.smartcare_util import get_schedule
from actions.utils.constants import DOCTOR_TEXT, DATE_TEXT, TIME_TEXT, APPT_INFO_SLOTS

logger = logging.getLogger(__name__)

class ActionAskConfirmChange(Action):

    def name(self) -> Text:
        return "action_ask_confirm_change"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            appt_id = tracker.get_slot("appt_id")
            schedule = get_schedule(appt_id)
            doctor_name = schedule.get("doctor_name")
            time = schedule.get("time")
            date = schedule.get("date")
            appt_info = ""
            if doctor_name:
                appt_info += DOCTOR_TEXT.format(doctor_name=doctor_name)
            if time:
                appt_info += ", " + TIME_TEXT.format(time=time) if doctor_name else TIME_TEXT.format(time=time)
            if date:
                appt_info += ", " + DATE_TEXT.format(date=date) if doctor_name or time else DATE_TEXT.format(date=date)
            
            events = []
            for s in APPT_INFO_SLOTS:
                events.append(SlotSet(s, None))
            dispatcher.utter_message(response="utter_ask_confirm_change", appt_info=appt_info)
            return events
        except Exception as ex:
            logger.exception(ex)
            return []


class ActionAskConfirmCancel(Action):

    def name(self) -> Text:
        return "action_ask_confirm_cancel"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            appt_id = tracker.get_slot("appt_id")
            schedule = get_schedule(appt_id)
            doctor_name = schedule.get("doctor_name")
            time = schedule.get("time")
            date = schedule.get("date")
            appt_info = ""
            if doctor_name:
                appt_info += DOCTOR_TEXT.format(doctor_name=doctor_name)
            if time:
                appt_info += ", " + TIME_TEXT.format(time=time) if doctor_name else TIME_TEXT.format(time=time)
            if date:
                appt_info += ", " + DATE_TEXT.format(date=date) if doctor_name or time else DATE_TEXT.format(date=date)
            
            events = []
            for s in APPT_INFO_SLOTS:
                events.append(SlotSet(s, None))
            dispatcher.utter_message(response="utter_ask_confirm_cancel", appt_info=appt_info)
            return events
        except Exception as ex:
            logger.exception(ex)
            return []