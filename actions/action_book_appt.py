import logging

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset, FollowupAction

from actions.utils.constants import DEFAULT_ID, SCHEDULE_FIELD, USER_FIELD
from actions.utils.smartcare_util import insert_user, insert_schedule

logger = logging.getLogger(__name__)


class ActionBookAppt(Action):

    def name(self) -> Text:
        return "action_book_appt"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            parent_id = tracker.current_state()["sender_id"]
            if tracker.get_slot("user_id") == DEFAULT_ID:
                patient_info = {
                    "parent_id": parent_id,
                    "smoker" : "",
                    "diabetes" : "",
                    "hypertension" : "",
                    "heart_problems" : ""
                }
                for f in USER_FIELD:
                    patient_info[f] = tracker.get_slot(f)
                user_id = insert_user(patient_info)

            else:
                user_id = tracker.get_slot("user_id")

            schedule = {
                "user_id": user_id,
                "parent_id": parent_id
            }
            for f in SCHEDULE_FIELD:
                schedule[f] = tracker.slots.get(f)

            schedule_id = insert_schedule(schedule)
            if schedule_id:
                dispatcher.utter_message(response="utter_payment")
            return []
        except Exception as ex:
            logger.exception(ex)
            return []
