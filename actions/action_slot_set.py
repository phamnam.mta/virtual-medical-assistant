import logging

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset

from actions.utils.constants import DEFAULT_ID, USER_FIELD
from actions.utils.smartcare_util import get_user_by_parent, get_user

logger = logging.getLogger(__name__)

class ActionPatientSlotSet(Action):

    def name(self) -> Text:
        return "action_patient_slot_set"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        try:
            events = []
            if tracker.get_slot("user_id") == DEFAULT_ID:
                for p in USER_FIELD:
                    events.append(SlotSet(p, None))
            else:
                user_id = tracker.get_slot("user_id")
                user_info = get_user(user_id)
                if user_info:
                    for p in USER_FIELD:
                        events.append(SlotSet(p, user_info.get(p)))
            return events
        except Exception as ex:
            logger.exception(ex)
            return []

class ActionBookSlotSet(Action):

    def name(self) -> Text:
        return "action_appt_slot_set"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]):
        try:
            entities = tracker.latest_message.get("entities", [])
            events = []
            labels = [e.get("entity") for e in entities]
            if entities:
                for e in entities:
                    if e.get("entity") == 'person_name':
                        events.append(SlotSet("doctor_name", e.get("value")))
                    elif e.get("entity") == 'date_time':
                        events.append(SlotSet("date", e.get("value")))
                    elif e.get("entity") == 'time':
                        events.append(SlotSet("time", e.get("value")))
                last_intent = tracker.latest_message.get("intent").get("name")
                if events and last_intent == "book_appt":
                    parent_id = tracker.current_state()["sender_id"]
                    events.append(SlotSet("user_id", parent_id))
                    if "date_time" in labels or "time" in labels:
                        events.append(SlotSet("with_doctor", False))
                    elif "person_name" in labels:
                        events.append(SlotSet("with_doctor", True))
            return events
        except Exception as ex:
            logger.exception(ex)
            return []
