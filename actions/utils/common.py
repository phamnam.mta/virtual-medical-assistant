import os
import logging
from typing import Dict, Text, List, Any, Callable
from rasa_sdk import Tracker

from actions.utils.constants import ACTION_CANCEL_TEXT, ACTION_CHANGE_TEXT

logger = logging.getLogger(__name__)

def get_last_appt_intent(tracker: Tracker):
    last_appt_intent = ""
    for e in reversed(tracker.events):
        if e["event"] == "user":
            if e["parse_data"]["intent"]["name"] == "change_appt":
                last_appt_intent = "change_appt"
                break
            elif e["parse_data"]["intent"]["name"] == "book_appt":
                last_appt_intent = "book_appt"
                break
            elif e["parse_data"]["intent"]["name"] == "cancel_appt":
                last_appt_intent = "cancel_appt"
                break
    return last_appt_intent


def get_bot_utter(tracker: Tracker):
    bot_utter = [event['metadata'].get('utter_action') for event in reversed(
        tracker.events) if event.get('event') == 'bot' and event['metadata'].get('utter_action')]
    return bot_utter

def elements_to_display(appt_list: List, last_appt_intent: Text):
    try:
        elements = []
        action_text = ACTION_CHANGE_TEXT.title()
        if last_appt_intent == "cancel_appt":
            action_text = ACTION_CANCEL_TEXT.title()
        for a in appt_list:
            if last_appt_intent == "cancel_appt":
                payload = f'/inform_cancel{{"appt_id": "{a["id"]}"}}'
            else:
                payload = f'/inform_change{{"appt_id": "{a["id"]}"}}'
            elements.append({
                "title": f"Tên bệnh nhân: {a['username']}" ,
                "subtitle": f"Tên bác sĩ: {a['doctor_name']}  \nNgày hẹn: {a['date']}  \nGiờ hẹn: {a['time_slot']}",
                "image_url": a['avatar'],
                "buttons": [
                    {
                        "type": "postback",
                        "title": action_text,
                        "payload": payload
                    }
                ]
            })

        return elements

    except Exception as ex:
        logger.exception(ex)

def time_slots_display(doctors: List):
    try:
        time_slots = []
        for d in doctors:
            time_slots.append({
                "title": d['doctor_name'],
                "subtitle": f"Chuyên khoa: {d['department']}  \nChức danh: {d['degree']}  \nPhí tư vấn: {d['consultant_fee']}",
                "image_url": d['avatar'],
                "buttons": [
                    {
                        "type": "postback",
                        "title": f"{slot['time']}({slot['date']})",
                        "payload": f'/inform{{"doctor_id": "{d["doctor_id"]}", "doctor_name": "{d["doctor_name"]}", "avatar": "{d["avatar"]}", "time_slot": "{slot["time"]}", "date": "{slot["date"]}", "fee": "{slot["fee"]}"}}'} for slot in d["time_slots"]
                ]
            })

        return time_slots
    except Exception as ex:
        logger.exception(ex)

def doctors_display(doctors: List):
    try:
        doctors_to_display = []
        for d in doctors:
            doctors_to_display.append({
                "title": d['doctor_name'],
                "subtitle": f"Chuyên khoa: {d['department']}  \nChức danh: {d['degree']}  \nPhí tư vấn: {d['consultant_fee']}",
                "image_url": d['avatar'],
                "buttons": [
                    {
                        "type": "postback",
                        "title": "Chọn bác sĩ",
                        "payload": f'/inform{{"doctor_id": "{d["doctor_id"]}", "doctor_name": "{d["doctor_name"]}"}}'
                    }
                ]
            })

        return doctors_to_display
    except Exception as ex:
        logger.exception(ex)