GENERIC_USER = "bạn"
DEFAULT_ID = "_"
SCHEDULE_FIELD = ["username", "gender", "birthday", "department", "time_slot", "note", "date", "doctor_id", "doctor_name", "avatar", "time", "fee"]
USER_FIELD = ["username", "gender", "birthday"]

ACTION_CHANGE_TEXT = "thay đổi"
ACTION_CANCEL_TEXT = "hủy"
FOR_YOU_TEXT = "bạn"
FOR_OTHER_TEXT = "người khám"

APPT_INFO_SLOTS = ["doctor_name", "time", "date"]
DOCTOR_TEXT = "với bác sĩ {doctor_name}"
TIME_TEXT = "vào lúc {time}"
DATE_TEXT = "ngày {date}"